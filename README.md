# Data-Stories

This is an open source project developing a collection of functions that will generate stunning images from data that has been prepared for the purpose from raw data sets relating to climate change. Data wrangled with Python, visualised with JS

Contributions to this project will be enabled after the set up phase, due to complete by 1st August 2021.  

## To contribute to this project:  

### 1.  Create your own branch
Clone this repository OR if you have already done this, **checkout the master branch and git pull** to get the latest version
Ensure that you are working in your own branch

### 2.  Package your contribution into a folder with the following format:  
Folder name  
  |--> a README.md with the name of the data set and a link to the raw data set online, your name as author, the data story, a caption for the data image, a link to a related webpage (something of interest related to the data story)  
  |--> a screenshot of the design for the image (if this folder will contain an image)  
  |--> code folder containing the code to create the image from the data set  
  |--> the final, created image (if this folder will contain an image)  

### 3.  Upload, or push, it to your branch
Your commit message should state that this is the 'First upload' or, if it it not the first upload, a short message describing what you have changed.  

### 4.  Create a new merge request to merge YOUR BRANCH into the DEVELOP branch
Your code will then be reviewed and merged
